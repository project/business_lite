/**
 * @file
 * Cycle activation the variables come from banners.inc.
 */

jQuery(document).ready(function($) {

  rotateBanners();

  function rotateBanners(){
    // Rotate banners.
    $('#header-images').cycle({
      slideExpr:'img.slide',
      pause:  parseInt(Drupal.settings.business_lite_configure_cycle.js.banner_pause),
    });
  }

  function onBefore() {
    var link = $(this).parent().attr("href");
    var title = $(this).attr("title");
    var description = $(this).attr("longdesc");

    // Set banner title.
    $('#header-image-title > a').text(title);
    // Set banner description.
    $('#header-image-description > a').text(description);
     // Set banner link on text.
    $('.bannerlink').attr({
      href: link
        });
  };

  // Hide banners when css is disabled.
  var banner = $("#header-images");
  var cycling = false;

  // If we can read this property, then we have css support.
  if ($("#pageBorder").css("float") == "left") {
    cycling = true;
    rotateBanners();
  }
  else {
    $('img.slide').hide();
  }

  // Check css support every second.
  var checkInterval = setInterval(function (banner){
    if ($("#pageBorder").css("float") == "none") {
      if (cycling) {
        $('img.slide').hide();
        banner.cycle("destroy");
        cycling = false;
      }
    }
    else {
      if (!cycling) {
        $('img.slide').show();
        rotateBanners();
        cycling = true;
      }
    }
  }, 1000, banner);
});
