/**
 * @file
 * The script is used to set image title and description.
 */

jQuery(document).ready(function($){
  var title    = $('img.slide').attr("title");
  var desc      = $('img.slide').attr("longdesc");
  // Set image title.
  $('#header-image-title').text(title);
  // Set image description.
  $('#header-image-description').text(desc);
});
