<?php

/**
 * @file
 * Overrides of theme implementations.
 */

/**
 * Function that renders classic primary menu with <h2>.
 */
function business_lite_theme() {
  return array(
    // Function that renders classic primary menu with <h2>.
    'custom_links' => array(
      'variables' => array(
        'links' => NULL,
        'attributes' => NULL,
        'heading' => NULL,
      ),
    ),
    'mega_menu' => array(
      'variables' => array('menu' => NULL),
    ),
    'mbanner_text' => array(
      // 'variables' => array('text' => NULL),.
    ),
    'mbanner_nav' => array(
      'variables' => array('prev' => NULL, 'next' => NULL),
    ),
  );
}

/**
 * Function that give text and description for banners.
 */
function business_lite_mbanner_text() {
  $banner_text  = '<div id="header-image-text" class="business_lite-hide-no-js">';
  $banner_text .= '<div id="header-image-text-data">';
  $banner_text .= '<' . OUTTAG . ' id="header-image-title"><a href="#" class="bannerlink" title="' . t('See this content') . '">title</a></' . OUTTAG . '>';
  $banner_text .= '<p id="header-image-description"><a href="#" class="bannerlink" title="' . t('See this content') . '">description</a></p>';
  $banner_text .= '</div>';
  $banner_text .= '</div>';

  return $banner_text;
}
